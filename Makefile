GITHUB_ORGS := containerd coredns fluent grpc kubernetes linkerd opentracing prometheus rkt containernetworking
GITHUB_REPOS := $(shell cat github/repos.txt  | sed -e :a -e '$!N; s/\n/ /; ta')
DOCKERHUB_ORGS := coredns linkerd fluent grpc prom
DOCKERHUB_REPOS := $(shell cat dockerhub/repos.txt  | sed -e :a -e '$!N; s/\n/ /; ta')
DOCKERHUB_TOKEN := $(shell curl -s -H "Content-Type: application/json" -X POST -d '{"username": "'${DOCKERHUB_USERNAME}'", "password": "'${DOCKERHUB_PASSWORD}'"}' https://hub.docker.com/v2/users/login/ | jq -r .token)
GITHUB_REPOS_WITH_RELEASES := $(shell cat github/repos_with_releases.txt  | sed -e :a -e '$!N; s/\n/ /; ta')
GITHUB_REPOS_WITH_ARTIFACTS := $(shell cat github/repos_with_artifacts.txt  | sed -e :a -e '$!N; s/\n/ /; ta')

data: dockerhub/repos.txt github/repos.txt github_releases dockerhub_tags github/repos_with_releases.txt github/repos_with_artifacts.txt

github/repos_with_releases.txt: github_releases
	find . -name releases.json -size +4b | awk -F/ '{print $$3"/"$$4}' | uniq | sort > $@

github/repos_with_artifacts.txt: github_releases
	grep browser_download_url github/*/*/releases.json | awk -F/ '{print $$7"/"$$8}' | uniq | sort > $@

clean:
	rm -rf gitlab/*/ dockerhub/*/

dockerhub/repos.txt: $(DOCKERHUB_ORGS:%=dockerhub/%/repos.json)
	@mkdir -p $(dir $@)
	@cat dockerhub/*/repos.json | jq -r '..|select(type=="array") | map([.user,.name])[] | join("/")' > $@

dockerhub/%/repos.json:
	@mkdir -p $(dir $@)
	@echo "curl -s <DOCKERHUB_AUTHENTICATION> https://hub.docker.com/v2/repositories/$*/?page_size=100 | jq -r '.results|.[]|.name' | jq . > $@"
	@curl -s -H "Content-Type: application/json" -H "Authorization: Bearer ${DOCKERHUB_TOKEN}" https://hub.docker.com/v2/repositories/$*/?page_size=100 | jq . > $@

github/repos.txt: $(GITHUB_ORGS:%=github/%/repos.json)
	@mkdir -p $(dir $@)
	@cat github/*/repos.json | jq -r '.[] | .full_name' > $@

github/%/repos.json:
	@mkdir -p $(dir $@)
	@echo "curl -sf -H 'Accept: application/vnd.github.v3+json' <GITHUB_AUTHENTICATION> https://api.github.com/orgs/$*/repos?per_page=100 | jq . > $@"
	@curl -sf -H 'Accept: application/vnd.github.v3+json' $(GITHUB_AUTHENTICATION) https://api.github.com/orgs/$*/repos?per_page=100 | jq . > $@

dockerhub_tags: $(DOCKERHUB_REPOS:%=dockerhub/%/tags.json)

dockerhub/%/tags.json:
	@mkdir -p $(dir $@)
	@echo "curl -s <DOCKERHUB_AUTHENTICATION> https://hub.docker.com/v2/repositories/$*/tags/?page_size=100 | jq . > $@"
	@curl -s -H "Content-Type: application/json" -H "Authorization: Bearer ${DOCKERHUB_TOKEN}" https://hub.docker.com/v2/repositories/$*/tags/?page_size=100 | jq . > $@

github_releases: $(GITHUB_REPOS:%=github/%/repo.json) $(GITHUB_REPOS:%=github/%/releases.json)

github/%/repo.json:
	@mkdir -p $(dir $@)
	@echo "curl -sf -H 'Accept: application/vnd.github.v3+json' <GITHUB_AUTHENTICATION> https://api.github.com/repos/$* | jq .> $@"
	@curl -sf -H 'Accept: application/vnd.github.v3+json' $(GITHUB_AUTHENTICATION) https://api.github.com/repos/$* | jq . > $@

github/%/releases.json:
	@mkdir -p $(dir $@)
	@echo "curl -sf -H 'Accept: application/vnd.github.v3+json' <GITHUB_AUTHENTICATION> https://api.github.com/repos/$*/releases | jq . > $@"
	@curl -sf -H 'Accept: application/vnd.github.v3+json' $(GITHUB_AUTHENTICATION) https://api.github.com/repos/$*/releases | jq . > $@

clean_empty:
	find . -empty | grep .json |xargs rm
